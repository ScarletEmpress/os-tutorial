#pragma once

/* Public kernel API */
void clear_screen();
void kprint_at(char *message, int col, int row);
void kprint(char* message);
void kprint_backspace();
