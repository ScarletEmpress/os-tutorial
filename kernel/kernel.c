#include "../drivers/screen.h"
#include "../cpu/isr.h"
#include "../libc/string.h"
#include "kernel.h"

int entry() {
    int success = 0; /* Explicit indication that ret 0 = good */

    isr_install();
    irq_install();

    kprint("Type something, it will go through the kernel\n"
        "Type END to halt CPU\n> ");

    return success;
}

void user_input(char *input) {
    if (strcmp(input, "END") == 0) {
        kprint("Stopping the CPU. Bye!\n");
        asm volatile("hlt");
    }
    //kprint("You said: ");
    //kprint(input);
    //kprint("\n> ");
    kprint("> ");
}
