[bits 32]
[extern entry] ; Define calling point. Must have the same name as kernel.c 'main' function.
call entry; Calls the C function. The linker will know where it is placed in memory.
jmp $
